﻿using System.IO;

namespace AmqpSend {
	internal class FileReader : IReader {
		public string ReadFile(string FileName) {
			if (!File.Exists(FileName)) {
				File.Create(FileName);
			}
			return File.ReadAllText(FileName);
		}
	}
}
