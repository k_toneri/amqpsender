﻿using System;
using System.Threading.Tasks;

namespace AmqpSend
{
    public static class Program
    {
        private const string TimeFormat = "yyyy-MM-ddTHH:mm:ss.fffK";
        private static IReader reader;

        private static bool loop = true;

        public static void Main(string[] args)
        {
            var messagePath = "Message.txt";

            reader = new FileReader();

            if (args.Length > 0 && args.Contains("-l"))
            {
                loop = true;
            }

            
            const int NumberOfPosters = 100;
            AmqpClient[] posters = CreateClientArray(NumberOfPosters);

            try
            {
                var message = reader.ReadFile(messagePath);

                int counter = 0;
                do
                {
                    Parallel.ForEach(posters, async (x) =>
                    {
                        await x.SendAsync(message);
                        Console.WriteLine(counter++);                        
                    }
                    );
                }
                while (loop);
            }
            catch (System.IO.FileNotFoundException exception)
            {
                Console.WriteLine(exception);
            }
                Console.WriteLine("Press RETURN to exit.");
                Console.ReadLine();
        }

        private static AmqpClient[] CreateClientArray(int nr)
        {
            var clients = new AmqpClient[nr];
            for (int i = 0; i < nr; i++)
            {
                clients[i] = new AmqpClient(reader.ReadFile("ConnectionString.txt"));
            }
            return clients;
        }

        private static bool Contains(this string[] arg, string searchTerm)
        {
            for (var i = 0; i < arg.Length; i++)
            {
                if (arg[i].Equals(searchTerm, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
