﻿namespace AmqpSend {
	internal interface IPoster {
		void Post(string message);
	}
}
