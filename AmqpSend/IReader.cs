﻿namespace AmqpSend {
	internal interface IReader {
		string ReadFile(string FileName);
	}
}
