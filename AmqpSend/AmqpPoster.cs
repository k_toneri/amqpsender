﻿using System.Threading.Tasks;

namespace AmqpSend {
	public class AmqpPoster : IPoster {
		private readonly string connectionString;

        public AmqpPoster(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Post(string message) {
			var amqpClient = new AmqpClient(connectionString);
			var send = Task.Run(() => amqpClient.SendAsync(message));
			send.Wait();
		}
	}
}
