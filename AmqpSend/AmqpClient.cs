﻿using Microsoft.Azure.EventHubs;
using System;
using System.Text;
using System.Threading.Tasks;

namespace AmqpSend {
	internal class AmqpClient {
		private readonly EventHubClient eventHubClient;
		private readonly string eventHubConnectionString;

		public AmqpClient(string connectionString) {
			eventHubConnectionString = connectionString;
			var connectionStringBuilder = new EventHubsConnectionStringBuilder(eventHubConnectionString);
			eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());
		}

		public void Close() => eventHubClient.Close();

		public async Task SendAsync(string json) {
			try {
				await eventHubClient.SendAsync(new EventData(Encoding.UTF8.GetBytes(json))).ConfigureAwait(false);
			}
			catch (TaskCanceledException) {
				Console.WriteLine($"Send canceled. Message:{Environment.NewLine}{json}");
			}
			catch (Exception ex) {
				Console.WriteLine(ex.Message);
			}
		}
	}
}
